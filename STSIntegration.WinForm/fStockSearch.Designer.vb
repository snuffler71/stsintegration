﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fStockSearch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgSTSStock = New System.Windows.Forms.DataGridView()
        Me.ManufacturerNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CategoryDescDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AvailableQuantityDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RetailPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Width = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ratio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Diameter = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StsStockCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StockDescDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SearchResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnSearchStock = New System.Windows.Forms.Button()
        Me.cbDeliveryAddress = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nudQuanity = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbManufacturers = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbProductTypes = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbTyreSizes = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbProductCategories = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblStockSearchHeading = New System.Windows.Forms.Label()
        Me.pnlStockSearch = New System.Windows.Forms.Panel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tbStockCode = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbSpeedRating = New System.Windows.Forms.TextBox()
        Me.tbLoadIndex = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cbRollingResistanceTo = New System.Windows.Forms.ComboBox()
        Me.cbRollingResistanceFrom = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbWetGripTo = New System.Windows.Forms.ComboBox()
        Me.cbWetGripFrom = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbNoisePerformanceTo = New System.Windows.Forms.ComboBox()
        Me.cbNoisePerformanceFrom = New System.Windows.Forms.ComboBox()
        Me.lvStockSearchDetail = New System.Windows.Forms.ListView()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.tbStockCodeOrder = New System.Windows.Forms.TextBox()
        Me.btnPlaceOrder = New System.Windows.Forms.Button()
        Me.tbStsSiteId = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        CType(Me.dgSTSStock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudQuanity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStockSearch.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgSTSStock
        '
        Me.dgSTSStock.AllowUserToAddRows = False
        Me.dgSTSStock.AllowUserToDeleteRows = False
        Me.dgSTSStock.AutoGenerateColumns = False
        Me.dgSTSStock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgSTSStock.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ManufacturerNameDataGridViewTextBoxColumn, Me.CategoryDescDataGridViewTextBoxColumn, Me.AvailableQuantityDataGridViewTextBoxColumn, Me.RetailPrice, Me.Width, Me.Ratio, Me.Diameter, Me.StsStockCode, Me.StockDescDataGridViewTextBoxColumn})
        Me.dgSTSStock.DataSource = Me.SearchResultBindingSource
        Me.dgSTSStock.Location = New System.Drawing.Point(23, 237)
        Me.dgSTSStock.Name = "dgSTSStock"
        Me.dgSTSStock.ReadOnly = True
        Me.dgSTSStock.Size = New System.Drawing.Size(963, 189)
        Me.dgSTSStock.TabIndex = 0
        '
        'ManufacturerNameDataGridViewTextBoxColumn
        '
        Me.ManufacturerNameDataGridViewTextBoxColumn.DataPropertyName = "ManufacturerName"
        Me.ManufacturerNameDataGridViewTextBoxColumn.HeaderText = "Manufacturer"
        Me.ManufacturerNameDataGridViewTextBoxColumn.Name = "ManufacturerNameDataGridViewTextBoxColumn"
        Me.ManufacturerNameDataGridViewTextBoxColumn.ReadOnly = True
        Me.ManufacturerNameDataGridViewTextBoxColumn.Width = 120
        '
        'CategoryDescDataGridViewTextBoxColumn
        '
        Me.CategoryDescDataGridViewTextBoxColumn.DataPropertyName = "CategoryDesc"
        Me.CategoryDescDataGridViewTextBoxColumn.HeaderText = "Category"
        Me.CategoryDescDataGridViewTextBoxColumn.Name = "CategoryDescDataGridViewTextBoxColumn"
        Me.CategoryDescDataGridViewTextBoxColumn.ReadOnly = True
        Me.CategoryDescDataGridViewTextBoxColumn.Width = 150
        '
        'AvailableQuantityDataGridViewTextBoxColumn
        '
        Me.AvailableQuantityDataGridViewTextBoxColumn.DataPropertyName = "AvailableQuantity"
        Me.AvailableQuantityDataGridViewTextBoxColumn.HeaderText = "Quantity"
        Me.AvailableQuantityDataGridViewTextBoxColumn.Name = "AvailableQuantityDataGridViewTextBoxColumn"
        Me.AvailableQuantityDataGridViewTextBoxColumn.ReadOnly = True
        Me.AvailableQuantityDataGridViewTextBoxColumn.Width = 50
        '
        'RetailPrice
        '
        Me.RetailPrice.DataPropertyName = "RetailPrice"
        Me.RetailPrice.HeaderText = "Retail Price"
        Me.RetailPrice.Name = "RetailPrice"
        Me.RetailPrice.ReadOnly = True
        '
        'Width
        '
        Me.Width.DataPropertyName = "Width"
        Me.Width.HeaderText = "W"
        Me.Width.Name = "Width"
        Me.Width.ReadOnly = True
        Me.Width.Width = 30
        '
        'Ratio
        '
        Me.Ratio.DataPropertyName = "Ratio"
        Me.Ratio.HeaderText = "R"
        Me.Ratio.Name = "Ratio"
        Me.Ratio.ReadOnly = True
        Me.Ratio.Width = 30
        '
        'Diameter
        '
        Me.Diameter.DataPropertyName = "Diameter"
        Me.Diameter.HeaderText = "D"
        Me.Diameter.Name = "Diameter"
        Me.Diameter.ReadOnly = True
        Me.Diameter.Width = 30
        '
        'StsStockCode
        '
        Me.StsStockCode.DataPropertyName = "StsStockCode"
        Me.StsStockCode.HeaderText = "Stock Code"
        Me.StsStockCode.Name = "StsStockCode"
        Me.StsStockCode.ReadOnly = True
        Me.StsStockCode.Width = 130
        '
        'StockDescDataGridViewTextBoxColumn
        '
        Me.StockDescDataGridViewTextBoxColumn.DataPropertyName = "StockDesc"
        Me.StockDescDataGridViewTextBoxColumn.HeaderText = "Stock Desccription"
        Me.StockDescDataGridViewTextBoxColumn.Name = "StockDescDataGridViewTextBoxColumn"
        Me.StockDescDataGridViewTextBoxColumn.ReadOnly = True
        Me.StockDescDataGridViewTextBoxColumn.Width = 280
        '
        'SearchResultBindingSource
        '
        Me.SearchResultBindingSource.DataSource = GetType(STSIntegration.Service.IntegrationService.SearchResult)
        '
        'btnSearchStock
        '
        Me.btnSearchStock.Location = New System.Drawing.Point(693, 155)
        Me.btnSearchStock.Name = "btnSearchStock"
        Me.btnSearchStock.Size = New System.Drawing.Size(228, 23)
        Me.btnSearchStock.TabIndex = 1
        Me.btnSearchStock.Text = "Search STS Stock"
        Me.btnSearchStock.UseVisualStyleBackColor = True
        '
        'cbDeliveryAddress
        '
        Me.cbDeliveryAddress.FormattingEnabled = True
        Me.cbDeliveryAddress.Location = New System.Drawing.Point(120, 8)
        Me.cbDeliveryAddress.Name = "cbDeliveryAddress"
        Me.cbDeliveryAddress.Size = New System.Drawing.Size(357, 21)
        Me.cbDeliveryAddress.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "DeliveryAddress"
        '
        'nudQuanity
        '
        Me.nudQuanity.Location = New System.Drawing.Point(563, 104)
        Me.nudQuanity.Name = "nudQuanity"
        Me.nudQuanity.Size = New System.Drawing.Size(46, 20)
        Me.nudQuanity.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(510, 106)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Quanity"
        '
        'cbManufacturers
        '
        Me.cbManufacturers.FormattingEnabled = True
        Me.cbManufacturers.Location = New System.Drawing.Point(120, 44)
        Me.cbManufacturers.Name = "cbManufacturers"
        Me.cbManufacturers.Size = New System.Drawing.Size(121, 21)
        Me.cbManufacturers.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Manufacturers"
        '
        'cbProductTypes
        '
        Me.cbProductTypes.FormattingEnabled = True
        Me.cbProductTypes.Location = New System.Drawing.Point(120, 72)
        Me.cbProductTypes.Name = "cbProductTypes"
        Me.cbProductTypes.Size = New System.Drawing.Size(355, 21)
        Me.cbProductTypes.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Product Types"
        '
        'cbTyreSizes
        '
        Me.cbTyreSizes.FormattingEnabled = True
        Me.cbTyreSizes.Location = New System.Drawing.Point(563, 12)
        Me.cbTyreSizes.Name = "cbTyreSizes"
        Me.cbTyreSizes.Size = New System.Drawing.Size(121, 21)
        Me.cbTyreSizes.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(493, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Tyre Sizes"
        '
        'cbProductCategories
        '
        Me.cbProductCategories.FormattingEnabled = True
        Me.cbProductCategories.Location = New System.Drawing.Point(120, 106)
        Me.cbProductCategories.Name = "cbProductCategories"
        Me.cbProductCategories.Size = New System.Drawing.Size(355, 21)
        Me.cbProductCategories.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 109)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(97, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Product Categories"
        '
        'lblStockSearchHeading
        '
        Me.lblStockSearchHeading.AutoSize = True
        Me.lblStockSearchHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStockSearchHeading.Location = New System.Drawing.Point(19, 9)
        Me.lblStockSearchHeading.Name = "lblStockSearchHeading"
        Me.lblStockSearchHeading.Size = New System.Drawing.Size(117, 20)
        Me.lblStockSearchHeading.TabIndex = 14
        Me.lblStockSearchHeading.Text = "Stock Search"
        '
        'pnlStockSearch
        '
        Me.pnlStockSearch.Controls.Add(Me.Label14)
        Me.pnlStockSearch.Controls.Add(Me.Label7)
        Me.pnlStockSearch.Controls.Add(Me.Label13)
        Me.pnlStockSearch.Controls.Add(Me.tbStockCode)
        Me.pnlStockSearch.Controls.Add(Me.Label12)
        Me.pnlStockSearch.Controls.Add(Me.Label11)
        Me.pnlStockSearch.Controls.Add(Me.tbSpeedRating)
        Me.pnlStockSearch.Controls.Add(Me.tbLoadIndex)
        Me.pnlStockSearch.Controls.Add(Me.Label10)
        Me.pnlStockSearch.Controls.Add(Me.cbRollingResistanceTo)
        Me.pnlStockSearch.Controls.Add(Me.cbRollingResistanceFrom)
        Me.pnlStockSearch.Controls.Add(Me.Label9)
        Me.pnlStockSearch.Controls.Add(Me.cbWetGripTo)
        Me.pnlStockSearch.Controls.Add(Me.cbWetGripFrom)
        Me.pnlStockSearch.Controls.Add(Me.Label8)
        Me.pnlStockSearch.Controls.Add(Me.cbNoisePerformanceTo)
        Me.pnlStockSearch.Controls.Add(Me.cbNoisePerformanceFrom)
        Me.pnlStockSearch.Controls.Add(Me.cbProductTypes)
        Me.pnlStockSearch.Controls.Add(Me.cbDeliveryAddress)
        Me.pnlStockSearch.Controls.Add(Me.btnSearchStock)
        Me.pnlStockSearch.Controls.Add(Me.Label2)
        Me.pnlStockSearch.Controls.Add(Me.Label6)
        Me.pnlStockSearch.Controls.Add(Me.nudQuanity)
        Me.pnlStockSearch.Controls.Add(Me.Label1)
        Me.pnlStockSearch.Controls.Add(Me.cbProductCategories)
        Me.pnlStockSearch.Controls.Add(Me.cbManufacturers)
        Me.pnlStockSearch.Controls.Add(Me.Label5)
        Me.pnlStockSearch.Controls.Add(Me.Label3)
        Me.pnlStockSearch.Controls.Add(Me.cbTyreSizes)
        Me.pnlStockSearch.Controls.Add(Me.Label4)
        Me.pnlStockSearch.Location = New System.Drawing.Point(23, 39)
        Me.pnlStockSearch.Name = "pnlStockSearch"
        Me.pnlStockSearch.Size = New System.Drawing.Size(963, 192)
        Me.pnlStockSearch.TabIndex = 15
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(885, 20)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(20, 13)
        Me.Label14.TabIndex = 30
        Me.Label14.Text = "To"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(808, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(30, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "From"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(270, 47)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(63, 13)
        Me.Label13.TabIndex = 28
        Me.Label13.Text = "Stock Code"
        '
        'tbStockCode
        '
        Me.tbStockCode.Location = New System.Drawing.Point(356, 45)
        Me.tbStockCode.Name = "tbStockCode"
        Me.tbStockCode.Size = New System.Drawing.Size(121, 20)
        Me.tbStockCode.TabIndex = 27
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(481, 78)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 13)
        Me.Label12.TabIndex = 26
        Me.Label12.Text = "Speed Rating"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(493, 47)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 13)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "Load Index"
        '
        'tbSpeedRating
        '
        Me.tbSpeedRating.Location = New System.Drawing.Point(563, 75)
        Me.tbSpeedRating.Name = "tbSpeedRating"
        Me.tbSpeedRating.Size = New System.Drawing.Size(100, 20)
        Me.tbSpeedRating.TabIndex = 24
        '
        'tbLoadIndex
        '
        Me.tbLoadIndex.Location = New System.Drawing.Point(563, 44)
        Me.tbLoadIndex.Name = "tbLoadIndex"
        Me.tbLoadIndex.Size = New System.Drawing.Size(100, 20)
        Me.tbLoadIndex.TabIndex = 23
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(692, 78)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(95, 13)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Rolling Resistance"
        '
        'cbRollingResistanceTo
        '
        Me.cbRollingResistanceTo.FormattingEnabled = True
        Me.cbRollingResistanceTo.Location = New System.Drawing.Point(873, 74)
        Me.cbRollingResistanceTo.Name = "cbRollingResistanceTo"
        Me.cbRollingResistanceTo.Size = New System.Drawing.Size(48, 21)
        Me.cbRollingResistanceTo.TabIndex = 21
        '
        'cbRollingResistanceFrom
        '
        Me.cbRollingResistanceFrom.FormattingEnabled = True
        Me.cbRollingResistanceFrom.Location = New System.Drawing.Point(802, 74)
        Me.cbRollingResistanceFrom.Name = "cbRollingResistanceFrom"
        Me.cbRollingResistanceFrom.Size = New System.Drawing.Size(48, 21)
        Me.cbRollingResistanceFrom.TabIndex = 20
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(738, 113)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Wet Grip"
        '
        'cbWetGripTo
        '
        Me.cbWetGripTo.FormattingEnabled = True
        Me.cbWetGripTo.Location = New System.Drawing.Point(873, 109)
        Me.cbWetGripTo.Name = "cbWetGripTo"
        Me.cbWetGripTo.Size = New System.Drawing.Size(48, 21)
        Me.cbWetGripTo.TabIndex = 18
        '
        'cbWetGripFrom
        '
        Me.cbWetGripFrom.FormattingEnabled = True
        Me.cbWetGripFrom.Location = New System.Drawing.Point(802, 109)
        Me.cbWetGripFrom.Name = "cbWetGripFrom"
        Me.cbWetGripFrom.Size = New System.Drawing.Size(48, 21)
        Me.cbWetGripFrom.TabIndex = 17
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(690, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(97, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Noise Performance"
        '
        'cbNoisePerformanceTo
        '
        Me.cbNoisePerformanceTo.FormattingEnabled = True
        Me.cbNoisePerformanceTo.Location = New System.Drawing.Point(873, 41)
        Me.cbNoisePerformanceTo.Name = "cbNoisePerformanceTo"
        Me.cbNoisePerformanceTo.Size = New System.Drawing.Size(48, 21)
        Me.cbNoisePerformanceTo.TabIndex = 15
        '
        'cbNoisePerformanceFrom
        '
        Me.cbNoisePerformanceFrom.FormattingEnabled = True
        Me.cbNoisePerformanceFrom.Location = New System.Drawing.Point(802, 41)
        Me.cbNoisePerformanceFrom.Name = "cbNoisePerformanceFrom"
        Me.cbNoisePerformanceFrom.Size = New System.Drawing.Size(48, 21)
        Me.cbNoisePerformanceFrom.TabIndex = 14
        '
        'lvStockSearchDetail
        '
        Me.lvStockSearchDetail.Location = New System.Drawing.Point(23, 451)
        Me.lvStockSearchDetail.Name = "lvStockSearchDetail"
        Me.lvStockSearchDetail.Size = New System.Drawing.Size(963, 240)
        Me.lvStockSearchDetail.TabIndex = 17
        Me.lvStockSearchDetail.UseCompatibleStateImageBehavior = False
        Me.lvStockSearchDetail.View = System.Windows.Forms.View.List
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(23, 712)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(87, 13)
        Me.Label15.TabIndex = 18
        Me.Label15.Text = "STS Stock Code"
        '
        'tbStockCodeOrder
        '
        Me.tbStockCodeOrder.Location = New System.Drawing.Point(116, 709)
        Me.tbStockCodeOrder.Name = "tbStockCodeOrder"
        Me.tbStockCodeOrder.Size = New System.Drawing.Size(204, 20)
        Me.tbStockCodeOrder.TabIndex = 19
        '
        'btnPlaceOrder
        '
        Me.btnPlaceOrder.Location = New System.Drawing.Point(536, 704)
        Me.btnPlaceOrder.Name = "btnPlaceOrder"
        Me.btnPlaceOrder.Size = New System.Drawing.Size(75, 23)
        Me.btnPlaceOrder.TabIndex = 20
        Me.btnPlaceOrder.Text = "Place Order"
        Me.btnPlaceOrder.UseVisualStyleBackColor = True
        '
        'tbStsSiteId
        '
        Me.tbStsSiteId.Location = New System.Drawing.Point(406, 707)
        Me.tbStsSiteId.Name = "tbStsSiteId"
        Me.tbStsSiteId.Size = New System.Drawing.Size(108, 20)
        Me.tbStsSiteId.TabIndex = 21
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(339, 712)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(61, 13)
        Me.Label16.TabIndex = 22
        Me.Label16.Text = "STS Site Id"
        '
        'fStockSearch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 754)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.tbStsSiteId)
        Me.Controls.Add(Me.btnPlaceOrder)
        Me.Controls.Add(Me.tbStockCodeOrder)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.lvStockSearchDetail)
        Me.Controls.Add(Me.pnlStockSearch)
        Me.Controls.Add(Me.lblStockSearchHeading)
        Me.Controls.Add(Me.dgSTSStock)
        Me.Name = "fStockSearch"
        Me.Text = "Stapletons Tyre Services"
        CType(Me.dgSTSStock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudQuanity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStockSearch.ResumeLayout(False)
        Me.pnlStockSearch.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgSTSStock As System.Windows.Forms.DataGridView
    Friend WithEvents btnSearchStock As System.Windows.Forms.Button
    Friend WithEvents cbDeliveryAddress As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents nudQuanity As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbManufacturers As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbProductTypes As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbTyreSizes As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbProductCategories As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblStockSearchHeading As System.Windows.Forms.Label
    Friend WithEvents pnlStockSearch As System.Windows.Forms.Panel
    Friend WithEvents cbNoisePerformanceFrom As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbNoisePerformanceTo As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cbRollingResistanceTo As System.Windows.Forms.ComboBox
    Friend WithEvents cbRollingResistanceFrom As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbWetGripTo As System.Windows.Forms.ComboBox
    Friend WithEvents cbWetGripFrom As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbSpeedRating As System.Windows.Forms.TextBox
    Friend WithEvents tbLoadIndex As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tbStockCode As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents SearchResultBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ManufacturerNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CategoryDescDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AvailableQuantityDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RetailPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Width As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ratio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Diameter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StsStockCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StockDescDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lvStockSearchDetail As System.Windows.Forms.ListView
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents tbStockCodeOrder As System.Windows.Forms.TextBox
    Friend WithEvents btnPlaceOrder As System.Windows.Forms.Button
    Friend WithEvents tbStsSiteId As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label

End Class
