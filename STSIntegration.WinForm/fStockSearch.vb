﻿Imports STSIntegration.Core
Imports System.ComponentModel
Imports STSIntegration.Service.IntegrationService

Public Class fStockSearch

#Region "Private Fields"
    Private ReadOnly stsRealTimeService As New STSRealTimeService()
    Private ReadOnly stsOvernightService As New STSOvernightService()

    Private deliveryAddressBindingSource As New BindingSource()
    Private manufacturers As List(Of Manufacturer)
    Private productTypes As List(Of ProductType)
    Private categoryTypes As List(Of ProductCategory)
    Private tyreSizes As List(Of TyreSize)
    Private tyreLabelRaatings As TyreLabelRatings
    Private searchResults As SearchResult()

#End Region

#Region "Form Events"

    Private Sub fStockSearch_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        BindOvernightData()
        SetUpDataGrid()
    End Sub

    Private Sub BtnSearchStock_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchStock.Click
        ''.DeliveryAddressId = cbDeliveryAddress.SelectedValue, _

        Dim model = New StockSearchModel _
                                                With {
                                                    .QuantityRequired = Decimal.Parse(nudQuanity.Value), _
                                                    .STSStockCode = tbStockCode.Text, _
                                                    .Size = cbTyreSizes.SelectedValue, _
                                                    .ProductTypeCode = cbProductTypes.SelectedValue, _
                                                    .ManufacturerCode = cbManufacturers.SelectedValue, _
                                                    .LoadIndex = tbLoadIndex.Text, _
                                                    .SpeedRating = tbSpeedRating.Text, _
                                                    .NoisePerformanceFrom = cbNoisePerformanceFrom.SelectedValue, _
                                                    .NoisePerformanceTo = cbNoisePerformanceTo.SelectedValue, _
                                                    .RollingResistanceFrom = cbRollingResistanceFrom.SelectedValue, _
                                                    .RollingResistanceTo = cbRollingResistanceTo.SelectedValue, _
                                                    .WetGripFrom = cbWetGripFrom.SelectedValue, _
                                                    .WetGripTo = cbWetGripTo.SelectedValue _
                                                }

        searchResults = stsRealTimeService.StockSearch(model)

        If searchResults IsNot Nothing AndAlso searchResults.Length > 0 Then
            dgSTSStock.DataSource = New BindingList(Of SearchResult)(searchResults.ToList())
        Else
            MessageBox.Show("No Results", "STS Stock Search")
        End If

    End Sub

    Private Sub cbProductTypes_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbProductTypes.SelectedIndexChanged
        Dim productTypeCode = cbProductTypes.SelectedValue

        If cbTyreSizes.DataSource IsNot Nothing AndAlso cbProductCategories.DataSource IsNot Nothing Then

            Dim filteredTyreSizes = tyreSizes.Where(Function(x) x.ProductTypeCode = productTypeCode).ToList()
            filteredTyreSizes.Insert(0, New TyreSize With {.WidthRatioDiameter = "Please Select"})
            cbTyreSizes.DataSource = filteredTyreSizes

            Dim filteredCategoryTypes = categoryTypes.Where(Function(x) x.ProductTypeCode = productTypeCode).ToList()
            filteredCategoryTypes.Insert(0, New ProductCategory With {.CategoryCode = String.Empty, .CategoryDesc = "All Categories"})
            cbProductCategories.DataSource = filteredCategoryTypes

        End If
    End Sub

    Private Sub dgSTSStock_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgSTSStock.CellClick
        ''Populate details of tyre
        Dim dataGrid = CType(sender, DataGridView)
        Dim stsStockCode = dataGrid.Rows(e.RowIndex).Cells("StsStockCode").Value
        lvStockSearchDetail.Clear()
        lvStockSearchDetail.AddDetails(searchResults.First(Function(x) x.StsStockCode = stsStockCode))

    End Sub

#End Region

#Region "Data Binding"

    Private Sub BindOvernightData()

        Try
            deliveryAddressBindingSource = New BindingSource() _
                With {.DataSource = New BindingList(Of AccountDeliveryAddress)(stsOvernightService.GetDeliveryAddressList().ToList())}
            cbDeliveryAddress.DisplayMember = "DeliveryAddress.AddressLine1"
            cbDeliveryAddress.ValueMember = "DeliveryAddressId"
            cbDeliveryAddress.DataSource = deliveryAddressBindingSource

            manufacturers = stsOvernightService.GetManufacturers().OrderBy(Function(x) x.ManufacturerDesc).ToList()
            manufacturers.Insert(0, New Manufacturer With {.ManufacturerCode = String.Empty, .ManufacturerDesc = "All Manufacturers"})
            cbManufacturers.DataSource = manufacturers
            cbManufacturers.DisplayMember = "ManufacturerDesc"
            cbManufacturers.ValueMember = "ManufacturerCode"

            productTypes = stsOvernightService.GetProductTypes().OrderBy(Function(x) x.ProductTypeDesc).ToList()
            productTypes.Insert(0, New ProductType With {.ProductTypeCode = String.Empty, .ProductTypeDesc = "All Product Types"})
            cbProductTypes.DataSource = productTypes
            cbProductTypes.DisplayMember = "ProductTypeDesc"
            cbProductTypes.ValueMember = "ProductTypeCode"

            categoryTypes = stsOvernightService.GetProductCategories().OrderBy(Function(x) x.CategoryDesc).ToList()
            categoryTypes.Insert(0, New ProductCategory With {.CategoryCode = String.Empty, .CategoryDesc = "All Categories"})
            cbProductCategories.DataSource = categoryTypes
            cbProductCategories.DisplayMember = "CategoryDesc"
            cbProductCategories.ValueMember = "CategoryCode"

            tyreSizes = stsOvernightService.GetTyreSizes().OrderBy(Function(x) x.WidthRatioDiameter).ToList()
            tyreSizes.Insert(0, New TyreSize With {.WidthRatioDiameter = "Please Select"})
            cbTyreSizes.DataSource = tyreSizes
            cbTyreSizes.DisplayMember = "WidthRatioDiameter"
            cbTyreSizes.ValueMember = "WidthRatioDiameter"

            tyreLabelRaatings = stsOvernightService.GetTyreLabelRatings()

            cbNoisePerformanceFrom.DataSource = tyreLabelRaatings.NoisePerformanceRatings.ToList()
            cbNoisePerformanceTo.DataSource = tyreLabelRaatings.NoisePerformanceRatings.ToList()
            cbNoisePerformanceFrom.SelectedIndex = -1
            cbNoisePerformanceTo.SelectedIndex = -1

            cbRollingResistanceFrom.DataSource = tyreLabelRaatings.RollingResistanceRatings.ToList()
            cbRollingResistanceTo.DataSource = tyreLabelRaatings.RollingResistanceRatings.ToList()
            cbRollingResistanceFrom.SelectedIndex = -1
            cbRollingResistanceTo.SelectedIndex = -1

            cbWetGripFrom.DataSource = tyreLabelRaatings.WetGripRatings.ToList()
            cbWetGripTo.DataSource = tyreLabelRaatings.WetGripRatings.ToList()
            cbWetGripFrom.SelectedIndex = -1
            cbWetGripTo.SelectedIndex = -1

        Catch ex As Exception

        End Try

    End Sub

#End Region

    Private Sub SetUpDataGrid()
        Me.dgSTSStock.Columns("RetailPrice").DefaultCellStyle.Format = "c"
    End Sub

    Private Sub btnPlaceOrder_Click(sender As System.Object, e As System.EventArgs) Handles btnPlaceOrder.Click
        Dim partNumber As String = tbStockCodeOrder.Text
        Dim stsSiteId As String = tbStsSiteId.Text

        Dim line As OrderRequestLine = New OrderRequestLine With {.STSStockCode = partNumber, .QuantityRequested = 1}
        Dim orderHeader As OrderRequestHeader = New OrderRequestHeader With {.Lines = New OrderRequestLine() {line}}
        Dim orderRequest As OrderRequest = New OrderRequest With {.Headers = New OrderRequestHeader() {orderHeader}}

        Dim stsSites As StsSite() = stsOvernightService.GetStsSites()

        orderHeader.STSSiteId = stsSiteId
        Dim placeOrderResult As PlaceOrderResult = stsRealTimeService.PlaceOrder(orderRequest)

        MessageBox.Show(placeOrderResult.Headers(0).StatusDescription)

    End Sub
End Class
