﻿Imports System.Runtime.CompilerServices
Imports STSIntegration.Service.IntegrationService

Public Module ListViewExtension

    <Extension()>
    Public Sub AddDetails(ByVal listView As ListView, ByRef detail As SearchResult)

        listView.Items.Add(String.Format("StsStockCode: {0}", detail.StsStockCode))
        listView.Items.Add(String.Format("Load rating: {0}", detail.LoadRating))
        listView.Items.Add(String.Format("Speed rating: {0}", detail.SpeedRating))
        listView.Items.Add(String.Format("Noise Performance: {0}", detail.LabelNoisePerformance))
        listView.Items.Add(String.Format("Rolling Resistance: {0}", detail.LabelRollingResistance))
        listView.Items.Add(String.Format("Wet Grip: {0}", detail.LabelWetGrip))
        listView.Items.Add(String.Format("STS Site Id: {0}", detail.StsSiteId))

    End Sub

End Module