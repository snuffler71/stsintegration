# STS Integration #

This README documents the work carried out with regard to integration with Stapletons Tyre Services.

The solution was developed using Visual Studio 2010 and .NET framework version 4.0 in VB .NET

### What is the solution structure? ###

The solution contains the following projects:

* STSIntegration.WinForm
* STSIntegration.Core
* STSIntegration.Service
* STSIntegration.Test

### STSIntegration.Service ###

This class library encapsulates the Service Reference for the STS Staging Web Service, situated here:

http://staging.stswebservices.co.uk/STSIntegrationService.svc

In addition to the STS web service the service reference also defines the classes that are required to interact with Stapletons,

### STSIntegration.Core ###

This class library defines the service classes that wrap the STS Service reference and expose functionality to clients via 2 main interfaces, as follows:

* **IRealTimeService** - Contract for functionality that will be run on an as-required basis, including: *StockSearch(), PlaceOrder(), GetOpenOrders(), CancelOrder(), GetAccountSettings()*

* **IOvernightService** - Contract for functionality that will be run when form loads:  *GetDeliveryAddressList(), GetManufacturers(), GetProductTypes(), GetProductCategories(), GetTyreSizes(), GetTyreLabelRatings(), UpdateStockCodeCrossReference()*

The concrete STS specific versions of these interfaces are implemented here:

* **STSRealTimeService**
* **STSOvernightService**

This project also defines some additional models to help clients integrate more easily with STS web service, they are:

* **BaseModel** - contains DeliveryAddressId and is base class for all models
* **OrderModel** - contains STSSiteId and STSOrderNo
* **OpenOrderModel** - contains CustomerReference, STSStockCode,IncludeBackOrders and IncludeLines
* **SearchModel** - contains numerous field that are used when searching for stock

Finally the project defines a number of status code constants, as follows:

* **HeaderStatusCodes** - Status code returned in the results of PlaceOrder(), such as *OrderPlaced* or *CreditlimitExceededed*
* **LineStatusCodes** - Status code returned from PlaceOrder(), e.g. *Valid* or *NoStockAvailable*
* **CancelStatusCodes** - Status code returned from CancelOrder(), e.g. *OrderCancelled* or *InvalidSTSOrderNo*

### STSIntegration.Test ###

This test project contains the following classes:

* **OvernightServiceShouldBeAbleTo** - Basic set of unit tests for OvernightService functionality
* **RealTimeServiceShouldBeAbleTo** -  Basic set of unit tests for RealTimeService functionality
* **STSIntegrationServiceShouldBeAbleTo** - Basic set of unit tests for direct calling of web service

### STSIntegration.WinForm###

This is the Windows Form application that will consume the RealTimeService and the OvernightService, specifically for Stock searching. It consists of the following forms:

* **fStockSearch** - Allows user to enter all the necessary stock search parameters to allow searching of STS. The search criteria is collected in a series of combo boxes, text boxes and numeric up-down controls. The results are shown DataGridView

* **fStockSearch.vb** - Behind the scenes the application instantiates the RealTime and Overnight services and on form load binds the overnight data to their relevant controls. It utilizes the .NET BindingSource and BindingList classes to allow filtering of data. On search it uses the RealTimeService.StockSearch() functionality to retrieve the requested data from STS.