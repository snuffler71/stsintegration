﻿
Public Class StockSearchModel
    Inherits BaseModel

    Public Property QuantityRequired As Int32
    Public Property STSStockCode As String
    Public Property Size As String
    Public Property ProductTypeCode As String
    Public Property ManufacturerCode As String
    Public Property LoadIndex As String
    Public Property SpeedRating As String
    Public Property NoisePerformanceFrom As String
    Public Property NoisePerformanceTo As String
    Public Property RollingResistanceFrom As String
    Public Property RollingResistanceTo As String
    Public Property WetGripFrom As String
    Public Property WetGripTo As String

End Class
