﻿Public Class OpenOrderModel
    Inherits OrderModel

    Public Property CustomerReference As String
    Public Property STSStockCode As String
    Public Property IncludeBackOrders As String
    Public Property IncludeLines As Boolean

End Class
