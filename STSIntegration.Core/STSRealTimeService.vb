﻿Imports STSIntegration.Service.IntegrationService

''' <summary>
''' STSRealTimeService encapsulates the functionality within STS Integration that is run on an as-required basis.
''' </summary>
''' <remarks></remarks>
Public Class STSRealTimeService
    Implements IRealTimeService

#Region "Private fields"

    Private _stsIntegrationService As STSIntegrationService
    Private _securityToken As String = "z+euAcnGOU3ByI6cG9P8+QF/IwyO+bMVpmvb40lYbfA=" 'TODO Retrieve this from app config or inject from Win Form app

#End Region

#Region "Constructor"

    Public Sub New(Optional securityToken As String = Nothing, Optional service As STSIntegrationService = Nothing)

        If (service Is Nothing) Then
            _stsIntegrationService = New STSIntegrationServiceClient() 'TODO Check that this pattern is ok, should it be created and destroyed in each method?
        Else
            _stsIntegrationService = service
        End If

        If (securityToken IsNot Nothing) Then
            _securityToken = securityToken
        End If

    End Sub

#End Region

#Region "Public Methods"

    Public Function StockSearch(stockSearchModel As StockSearchModel) As SearchResult() Implements IRealTimeService.StockSearch

        Return _stsIntegrationService.StockSearch(_securityToken, stockSearchModel.DeliveryAddressId,
                                                  stockSearchModel.QuantityRequired, stockSearchModel.STSStockCode,
                                                  stockSearchModel.Size, stockSearchModel.ProductTypeCode,
                                                  stockSearchModel.ManufacturerCode, stockSearchModel.LoadIndex,
                                                  stockSearchModel.SpeedRating, stockSearchModel.NoisePerformanceFrom,
                                                  stockSearchModel.NoisePerformanceTo,
                                                  stockSearchModel.RollingResistanceFrom,
                                                  stockSearchModel.RollingResistanceTo, stockSearchModel.WetGripFrom,
                                                  stockSearchModel.WetGripTo)
    End Function

    Public Function PlaceOrder(orderRequest As OrderRequest) As PlaceOrderResult Implements IRealTimeService.PlaceOrder
        Return _stsIntegrationService.PlaceOrder(_securityToken, String.Empty, orderRequest)
    End Function

    Public Function GetOpenOrders(openOrderModel As OpenOrderModel) Implements IRealTimeService.GetOpenOrders

        Return _stsIntegrationService.GetOpenOrders(_securityToken, openOrderModel.DeliveryAddressId,
                                                    openOrderModel.STSSiteId, openOrderModel.STSOrderNo,
                                                    openOrderModel.CustomerReference, openOrderModel.STSStockCode,
                                                    openOrderModel.IncludeBackOrders, openOrderModel.IncludeLines)

    End Function

    Public Function CancelOrder(orderModel As OrderModel) Implements IRealTimeService.CancelOrder
        Return _stsIntegrationService.CancelOrder(_securityToken, orderModel.STSSiteId, orderModel.STSOrderNo)
    End Function

    Public Function GetAccountSettings() As AccountSettings Implements IRealTimeService.GetAccountSettings
        Return _stsIntegrationService.GetAccountSettings(_securityToken)
    End Function

#End Region

End Class
