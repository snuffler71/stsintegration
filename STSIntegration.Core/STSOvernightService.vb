﻿Imports STSIntegration.Service.IntegrationService

''' <summary>
''' STSOvernightService encapsulates functionality that will be called infrequently, possibly once daily
''' </summary>
''' <remarks></remarks>
Public Class STSOvernightService
    Implements IOvernightService

#Region "Private fields"

    Private _stsIntegrationService As STSIntegrationService
    Private _securityToken As String = "z+euAcnGOU3ByI6cG9P8+QF/IwyO+bMVpmvb40lYbfA=" 'TODO Retrieve this from app config or inject from Win Form app?

#End Region

#Region "Constructor"

    Public Sub New(Optional securityToken As String = Nothing, Optional service As STSIntegrationService = Nothing)

        If (service Is Nothing) Then
            _stsIntegrationService = New STSIntegrationServiceClient() 'TODO Check that this pattern is ok, should it be created and destroyed in each method?
        Else
            _stsIntegrationService = service
        End If

        If (securityToken IsNot Nothing) Then
            _securityToken = securityToken
        End If

    End Sub

#End Region

#Region "Public Methods"

    Public Function GetDeliveryAddressList() As AccountDeliveryAddress() Implements IOvernightService.GetDeliveryAddressList
        Return _stsIntegrationService.GetDeliveryAddressList(_securityToken)
    End Function

    Public Function GetManufacturers() As Manufacturer() Implements IOvernightService.GetManufacturers
        Return _stsIntegrationService.GetManufacturers(_securityToken)
    End Function

    Public Function GetProductTypes() As ProductType() Implements IOvernightService.GetProductTypes
        Return _stsIntegrationService.GetProductTypes(_securityToken)
    End Function

    Public Function GetProductCategories() As ProductCategory() Implements IOvernightService.GetProductCategories
        Return _stsIntegrationService.GetProductCategories(_securityToken)
    End Function

    Public Function GetTyreSizes() As TyreSize() Implements IOvernightService.GetTyreSizes
        Return _stsIntegrationService.GetTyreSizes(_securityToken)
    End Function

    Public Function GetTyreLabelRatings() As TyreLabelRatings Implements IOvernightService.GetTyreLabelRatings
        Return _stsIntegrationService.GetTyreLabelRatings(_securityToken)
    End Function

    Public Function UpdateStockCodeCrossReference(headOfficeAccount As String,
                                                  updateStockCodeCrossReferenceRequest As UpdateStockCodeCrossReferenceRequest) As StockCodeCrossReference Implements IOvernightService.UpdateStockCodeCrossReference
        Return _stsIntegrationService.UpdateStockCodeCrossReference(_securityToken, headOfficeAccount, updateStockCodeCrossReferenceRequest)
    End Function

    Public Function GetStsSites() As StsSite() Implements IOvernightService.GetStsSites
        Return _stsIntegrationService.GetStsSites(_securityToken)
    End Function

#End Region

End Class
