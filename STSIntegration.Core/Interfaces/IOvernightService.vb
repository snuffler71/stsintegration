﻿Imports STSIntegration.Service.IntegrationService

Public Interface IOvernightService
    Function GetDeliveryAddressList() As AccountDeliveryAddress()
    Function GetManufacturers() As Manufacturer()
    Function GetProductTypes() As ProductType()
    Function GetProductCategories() As ProductCategory()
    Function GetTyreSizes() As TyreSize()
    Function GetTyreLabelRatings() As TyreLabelRatings
    Function UpdateStockCodeCrossReference(headOfficeAccount As String,
                                              updateStockCodeCrossReferenceRequest As UpdateStockCodeCrossReferenceRequest) As StockCodeCrossReference
    Function GetStsSites() As StsSite()
End Interface
