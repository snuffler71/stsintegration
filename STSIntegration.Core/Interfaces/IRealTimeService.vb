﻿Imports STSIntegration.Service.IntegrationService

Public Interface IRealTimeService

    Function StockSearch(stockSearchModel As StockSearchModel) As SearchResult()

    Function PlaceOrder(orderRequest As OrderRequest) As PlaceOrderResult

    Function GetOpenOrders(openOrderModel As OpenOrderModel)

    Function CancelOrder(orderModel As OrderModel)

    Function GetAccountSettings() As AccountSettings

End Interface
