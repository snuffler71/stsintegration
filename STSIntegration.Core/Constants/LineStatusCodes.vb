﻿Public Class LineStatusCodes
    Public Const Valid As String = "V"
    Public Const StockCodeInvalid As String = "0"
    Public Const NoStockAvailable As String = "1"
    Public Const NotWithinTolerance As String = "2"
    Public Const RequestedQuantityExceedsMaximumAllowed As String = "3"
End Class
