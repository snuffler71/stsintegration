﻿Public Class CancelStatusCodes
    Public Const InvalidSession As String = "0"
    Public Const InvalidSTSOrderNo As String = "1"
    Public Const UnableToCancelOrderIsReadyForDispatch As String = "2"
    Public Const UnableToCancelOrderHasBeenInvoiced As String = "3"
    Public Const UnableToCancelOrderAlreadyCancelled As String = "4"
    Public Const UnableToCancel As String = "5"
    Public Const UnableToCancelOrderBeginProcessing As String = "6"
    Public Const OrderCancelled As String = "C"
    Public Const OrderAmended As String = "A"
End Class
