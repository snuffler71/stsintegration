﻿Imports System.Text
Imports STSIntegration.Core
Imports STSIntegration.Service.IntegrationService

<TestClass()>
Public Class RealTimeServiceShouldBeAbleTo

    Private _realTimeService As IRealTimeService

    <TestInitialize()>
    Public Sub Initialize()
        _realTimeService = New STSRealTimeService()
    End Sub

    <TestMethod()>
    Public Sub SearchStockWithNoCriteria()

        Dim stockSearchModel = New StockSearchModel With {.QuantityRequired = 1}
        Dim results = _realTimeService.StockSearch(stockSearchModel)

        Assert.IsNotNull(results)
    End Sub

    <TestMethod()>
    Public Sub PlaceSingleOrder()

        Dim orderRequest As New OrderRequest With {.Headers = New OrderRequestHeader() {CreateOrderRequestHeaderWithOneLine()}}
        Dim results = _realTimeService.PlaceOrder(orderRequest)

        Assert.IsNotNull(results)
        Assert.IsTrue(results.Headers(0).StatusCode = HeaderStatusCodes.OrderPlaced)

    End Sub

    <TestMethod()>
    Public Sub PlaceOrderWithMultipleLines()

        Dim orderRequestHeader = CreateOrderRequestHeaderWithOneLine()
        Dim Line2 As New OrderRequestLine With {.QuantityRequested = 1}
        orderRequestHeader.Lines.Resize(orderRequestHeader.Lines, orderRequestHeader.Lines.Length + 1)
        orderRequestHeader.Lines(1) = Line2

        Dim orderRequest As New OrderRequest With {.Headers = New OrderRequestHeader() {orderRequestHeader}}

        Dim results = _realTimeService.PlaceOrder(orderRequest)

        Assert.IsNotNull(results)
        Assert.IsTrue(results.Headers(0).StatusCode = HeaderStatusCodes.OrderPlaced)
    End Sub

    <TestMethod()>
    Public Sub PlaceMultipleOrdersWithMultipleLines()

        Dim orderRequestHeader1 = CreateOrderRequestHeaderWithOneLine()
        Dim Line2 As New OrderRequestLine With {.QuantityRequested = 1}
        orderRequestHeader1.Lines.Resize(orderRequestHeader1.Lines, orderRequestHeader1.Lines.Length + 1)
        orderRequestHeader1.Lines(1) = Line2

        Dim orderRequestHeader2 = CreateOrderRequestHeaderWithOneLine()
        Dim Line3 As New OrderRequestLine With {.QuantityRequested = 1}
        orderRequestHeader2.Lines.Resize(orderRequestHeader2.Lines, orderRequestHeader2.Lines.Length + 1)
        orderRequestHeader2.Lines(1) = Line3

        Dim orderRequest As New OrderRequest With {.Headers = New OrderRequestHeader() {orderRequestHeader1, orderRequestHeader2}}

        Dim results = _realTimeService.PlaceOrder(orderRequest)

        Assert.IsNotNull(results)
        Assert.IsTrue(results.Headers(0).StatusCode = HeaderStatusCodes.OrderPlaced)
        Assert.IsTrue(results.Headers(1).StatusCode = HeaderStatusCodes.OrderPlaced)
    End Sub

    <TestMethod()>
    Public Sub GetOpenOrders()

        Dim openOrdermodel As New OpenOrderModel
        Dim results = _realTimeService.GetOpenOrders(openOrdermodel)

        Assert.IsNotNull(results)

    End Sub

    <TestMethod()>
    Public Sub CancelOrder()

        Dim ordermodel As New OrderModel With {.STSOrderNo = "12345678", .STSSiteId = "123"}
        Dim results = _realTimeService.CancelOrder(OrderModel)

        Assert.IsNotNull(results)

    End Sub

    <TestMethod()>
    Public Sub GetAccountSettings()
        Dim results = _realTimeService.GetAccountSettings()

        Assert.IsNotNull(results)

    End Sub

    Private Function CreateOrderRequestHeaderWithOneLine()
        Dim Line As New OrderRequestLine With {.QuantityRequested = 1}
        Dim orderRequestheader As New OrderRequestHeader
        orderRequestheader.Lines = {Line}
        Return orderRequestheader
    End Function

End Class
