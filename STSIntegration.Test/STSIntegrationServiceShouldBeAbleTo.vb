﻿Imports System.Text
Imports STSIntegration.Service.IntegrationService
Imports STSIntegration.Core

<TestClass()>
Public Class STSIntegrationServiceShouldBeAbleTo
    Private _securityToken As String = "z+euAcnGOU3ByI6cG9P8+QF/IwyO+bMVpmvb40lYbfA="

    <TestMethod()>
    Public Sub SearchStockUsingSecurityToken()
        Dim client As STSIntegrationServiceClient = New STSIntegrationServiceClient()

        Dim result = client.StockSearch(_securityToken, Nothing, 1, Nothing, "2055516", Nothing,
                                Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

        Assert.IsNotNull(result)

    End Sub

    <TestMethod()>
    Public Sub SearchStockAsyncUsingSecurityToken()
        Dim client As STSIntegrationServiceClient = New STSIntegrationServiceClient()

        AddHandler client.StockSearchCompleted, AddressOf StockSearchComplete
        client.StockSearchAsync(_securityToken, Nothing, 10, Nothing, Nothing, Nothing,
                                Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

    End Sub

    Private Shared Sub StockSearchComplete(ByVal sender As Object, ByVal e As StockSearchCompletedEventArgs)
        Console.WriteLine("Add Result: {0}", e.Result)
    End Sub

End Class
