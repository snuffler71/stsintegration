﻿Imports System.Text
Imports STSIntegration.Core
Imports STSIntegration.Service.IntegrationService

<TestClass()>
Public Class OvernightServiceShouldBeAbleTo

    Private _overnightService As IOvernightService

    <TestInitialize()>
    Public Sub Initialize()
        _overnightService = New STSOvernightService()
    End Sub

    <TestMethod()>
    Public Sub GetDeliveryAddressList()
        Dim result = _overnightService.GetDeliveryAddressList()
        Assert.IsNotNull(result)
    End Sub

    <TestMethod()>
    Public Sub GetManufacturers()
        Dim result = _overnightService.GetManufacturers()
        Assert.IsNotNull(result)
    End Sub

    <TestMethod()>
    Public Sub GetProductTypes()
        Dim result = _overnightService.GetProductTypes()
        Assert.IsNotNull(result)
    End Sub

    <TestMethod()>
    Public Sub GetProductCategories()
        Dim result = _overnightService.GetProductCategories()
        Assert.IsNotNull(result)
    End Sub

    <TestMethod()>
    Public Sub GetTyreSizes()
        Dim result = _overnightService.GetTyreSizes()
        Assert.IsNotNull(result)
    End Sub

    <TestMethod()>
    Public Sub GetTyreLabelRatings()
        Dim result = _overnightService.GetTyreLabelRatings()
        Assert.IsNotNull(result)
    End Sub

    <TestMethod()>
    Public Sub UpdateStockCodeCrossReference()
        Dim headOfficeAccount As String = ""
        Dim updateStockCodeCrossReferenceRequest As New UpdateStockCodeCrossReferenceRequest With {.STSStockCode = ""}
        Dim result = _overnightService.UpdateStockCodeCrossReference(headOfficeAccount, updateStockCodeCrossReferenceRequest)

        Assert.IsNotNull(result)
    End Sub

End Class
